package org.automatedtrade.recordstream;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.automatedtrade.reuse.properties.CommandLineProperties;
import org.automatedtrade.reuse.properties.PropertiesAssemble;

//import com.google.gson.Gson;

import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;

public class WriteTradingData implements ConnectionEventListener, ChannelEventListener {

    /*
     * Parameters to set
     * 
     * --file what file to write to. If not supplied no writing to file
     * --stoptime date and time to run until. Default never/forever
     * --dateformat your alternate date format. Default yyyy-MM-dd HH:mm:ss
     * --dumptoscreen true|false weather to write the lines to screen also. Default true.
     * --apikey Code from bitstamps api
     * 
     * Examples
     * 
     * --file /Users/karejonsson/tradedata/3.trade --stoptime '2017-05-13 09:40:03'
     * --file /Users/karejonsson/tradedata/4.trade --stoptime '20170513 100000' --dateformat 'yyyyMMdd HHmmss' --dumptoscreen true
     * 
     */

    public static void main(final String[] args) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
			//clp.printEvaluated();
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}
	
	public static void main(final CommandLineProperties clp) throws Exception {
		clp.printEvaluated();
		
		String filename = clp.getString("file");
		FileWriter fw = null;
		if(filename != null && filename.trim().length() > 3) {
			fw = new FileWriter(filename);
			fw.write("streamstart "+System.currentTimeMillis()+"\n");
		}
		
		long stop = Long.MAX_VALUE;
		String stoptime = clp.getString("stoptime");
		
		String dateformat = clp.getString("dateformat", "yyyy-MM-dd HH:mm:ss");
		
		if(stoptime != null) {
			DateFormat formatter = new SimpleDateFormat(dateformat); 
			Date date = (Date)formatter.parse(stoptime);			
			stop = date.getTime();
		}
		
		boolean dumpToScreen = clp.getBoolean("dumptoscreen", true);
		String apikey = clp.getString("apikey", "de504dc5763aeef9ff52");
		
    	WriteTradingData trades = new WriteTradingData(apikey, "live_trades", "trade", fw, dumpToScreen);
    	trades.run();
    	WriteTradingData orders = new WriteTradingData(apikey, "order_book", "data", fw, dumpToScreen);
    	orders.run();
    	WriteTradingData book = new WriteTradingData(apikey, "diff_order_book", "data", fw, dumpToScreen);
    	book.run();
    	WriteTradingData order_created = new WriteTradingData(apikey, "live_orders", "order_created", fw, dumpToScreen);
    	order_created.run();
    	WriteTradingData order_changed = new WriteTradingData(apikey, "live_orders", "order_changed", fw, dumpToScreen);
    	order_changed.run();
    	WriteTradingData order_deleted = new WriteTradingData(apikey, "live_orders", "order_deleted", fw, dumpToScreen);
    	order_deleted.run();

    	while (true) {
            try {
                Thread.sleep(1000);
            }
            catch (final InterruptedException e) {
                e.printStackTrace();
            }
            if(System.currentTimeMillis() > stop) {
				if(fw != null) {
					fw.write("streamend "+System.currentTimeMillis()+"\n");
	            	fw.flush();
	            	fw.close();
				}
            	System.exit(0);
            }
        }
    }
    
    private String apiKey;
    private String channelName;
    private String eventName;
    private PusherOptions options = null;
    private Pusher pusher = null;
    private FileWriter out = null;
    private boolean dumpToScreen = false;

    public WriteTradingData(final String apiKey, String channelName, String eventName, FileWriter out, boolean dumpToScreen) {
    	this.apiKey = apiKey;
    	this.channelName = channelName;
    	this.eventName = eventName;
    	this.out = out;
    	this.dumpToScreen = dumpToScreen;
        options = new PusherOptions().setEncrypted(true);
    }
        
    public void run() {
        pusher = new Pusher(apiKey, options);
        pusher.connect(this);
        pusher.subscribe(channelName, this, eventName);
    }

    private void strategyOnError() {
		try { pusher.disconnect(); } catch(Exception e) {}
    	while(true) {
    		try {
    			Thread.sleep(Math.max(0, 500));
    		} 
    		catch (InterruptedException e) {
    			e.printStackTrace();
    		}
        	if(pusher != null) {
        		try { pusher.disconnect(); } catch(Exception e) {}
        		pusher = null;
        	}
        	run();
    		try {
    			Thread.sleep(Math.max(0, 500));
    		} 
    		catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    		for(int i = 0 ; i < 6 ; i++) {
        		if(pusher.getConnection().getState() == ConnectionState.CONNECTING) {
            		try {
            			Thread.sleep(Math.max(0, 500));
            		} 
            		catch (InterruptedException e) {
            			e.printStackTrace();
            		}
        		}
    		}
    		if(pusher.getConnection().getState() == ConnectionState.CONNECTED) {
    			break;
    		}
    	}
    }

    public void onEvent(String channelName, String eventName, String data) {
        //final Map<String, String> jsonObject = gson.fromJson(data, Map.class);
    	String log = String.format("event %s, %s, %d, %s\n", channelName, eventName, timestamp(), data);
    	try {
			if(out != null) {
				out.write(log);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    	if(dumpToScreen) {
    		System.out.println(log);
    	}
	}

	public void onSubscriptionSucceeded(String paramString) {
		String log = String.format("subscriptionsucceeded [%d], [%s]\n", timestamp(), channelName);
    	try {
			if(out != null) {
				out.write(log);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    	if(dumpToScreen) {
    		System.out.println(log);
    	}
	}

	public void onConnectionStateChange(ConnectionStateChange change) {
		String log = String.format("connectionstatechange [%d], [%s], [%s]\n", timestamp(), change.getPreviousState(), change.getCurrentState());
    	try {
			if(out != null) {
				out.write(log);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
    	if(dumpToScreen) {
    		System.out.println(log);
    	}
	}

	public void onError(String message, String code, Exception e) {
		String log = String.format("error [%d], [%s], [%s], [%s]\n", timestamp(), message, code, e);
    	try {
			if(out != null) {
				out.write(log);
			}
		} catch (IOException ee) {
			ee.printStackTrace();
		}
    	if(dumpToScreen) {
    		System.out.println(log);
    	}
    	strategyOnError();
	}
	
    private long timestamp() {
        return System.currentTimeMillis();
    }

}