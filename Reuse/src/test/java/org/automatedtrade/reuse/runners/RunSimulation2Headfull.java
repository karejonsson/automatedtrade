package org.automatedtrade.reuse.runners;

import org.automatedtrade.reuse.show.DurationVariableManagement;
import org.automatedtrade.reuse.show.DurationVariableManagementHeadfull;
import org.automatedtrade.reuse.show.MomentaryVariableManagement;
import org.automatedtrade.reuse.show.MomentaryVariableManagementHeadfull;
import org.automatedtrade.reuse.show.SignalProcessingForVariables;
import org.automatedtrade.reuse.struct.StreamEventPropagator;
import org.automatedtrade.reuse.struct.TimeSimulationFast;
import org.automatedtrade.reuse.struct.TimeManager;
import org.automatedtrade.reuse.struct.TimeSystem;

public class RunSimulation2Headfull {

	public static void main(String args[]) {
		TimeManager tm = new TimeSimulationFast();
		StreamEventPropagator sel = RunnerReuse.getPropagatorFromFileResource(tm, "20170521_1452_4h.longtrade");

		DurationVariableManagement[] gms = new DurationVariableManagement[3];
		int fraction = 10;
		long minute = 60000;
		
		gms[0] = new DurationVariableManagementHeadfull(10*minute, 10*(minute/fraction), "Ten minute graphs");
		gms[1] = new DurationVariableManagementHeadfull(60*minute, 60*(minute/fraction), "One hour graphs");
		gms[2] = new DurationVariableManagementHeadfull(4*60*minute, 4*60*(minute/fraction), "Four hour graphs");
		MomentaryVariableManagement mgm = new MomentaryVariableManagementHeadfull("Momentary bid/ask situation");

		SignalProcessingForVariables vop = new SignalProcessingForVariables(tm, gms, mgm);
		sel.addOrderBooksListener(vop);
		sel.addTradeListener(vop);

		RunnerReuse.infiniteWait();		
	}

}
