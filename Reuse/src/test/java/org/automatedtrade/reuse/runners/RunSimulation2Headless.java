package org.automatedtrade.reuse.runners;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

import org.automatedtrade.reuse.setup.SetupFactory;
import org.automatedtrade.reuse.show.DurationVariableManagement;
import org.automatedtrade.reuse.show.DurationVariableManagementHeadfull;
import org.automatedtrade.reuse.show.DurationVariableManagementHeadless;
import org.automatedtrade.reuse.show.MomentaryVariableManagement;
import org.automatedtrade.reuse.show.MomentaryVariableManagementHeadfull;
import org.automatedtrade.reuse.show.MomentaryVariableManagementHeadless;
import org.automatedtrade.reuse.show.SignalProcessingForVariables;
import org.automatedtrade.reuse.struct.StreamEventPropagator;
import org.automatedtrade.reuse.struct.TimeManager;
import org.automatedtrade.reuse.struct.TimeSimulationFast;
import org.automatedtrade.reuse.struct.TimeSystem;

public class RunSimulation2Headless {

	public static void main(String args[]) {
		TimeManager tm = new TimeSimulationFast();
		StreamEventPropagator sel = RunnerReuse.getPropagatorFromFileResource(tm, "2.trade");

		DurationVariableManagement[] gms = new DurationVariableManagement[3];
		gms[0] = new DurationVariableManagementHeadless(10*60*1000, 10*60*10);
		gms[1] = new DurationVariableManagementHeadless(60*60*1000, 60*60*10);
		gms[2] = new DurationVariableManagementHeadless(4*60*60*1000, 4*60*60*10);
		MomentaryVariableManagement mgm = new MomentaryVariableManagementHeadless();

		SignalProcessingForVariables vop = new SignalProcessingForVariables(tm, gms, mgm);
		sel.addOrderBooksListener(vop);
		sel.addTradeListener(vop);

		RunnerReuse.infiniteWait();		
	}

}
