package org.automatedtrade.reuse.runners;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.automatedtrade.reuse.setup.SetupFactory;
import org.automatedtrade.reuse.struct.StreamEventPropagator;
import org.automatedtrade.reuse.struct.TimeManager;

public class RunnerReuse {
	
	public static StreamEventPropagator getPropagatorFromFileResource(TimeManager tm, String file) {
		ClassLoader classLoader = RunnerReuse.class.getClassLoader();
		InputStream is = classLoader.getResourceAsStream(file);
	    InputStreamReader isr = new InputStreamReader(is, Charset.forName("UTF-8"));
	    BufferedReader br = new BufferedReader(isr);
	    StreamEventPropagator sel = null;
		try {
			sel = SetupFactory.setupSimulation(tm, br);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return sel;
	}

	public static StreamEventPropagator getPropagatorFromBitstamp(String apiKey) {
	    StreamEventPropagator sel = SetupFactory.setupLive(apiKey);
		return sel;
	}

	public static void infiniteWait() {
		while(true) {
            try {
                Thread.sleep(1000);
            }
            catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }
	}

	public static StreamEventPropagator getPropagatorFromZippedFileResource(TimeManager tm, String file) throws IOException {
		ClassLoader classLoader = RunnerReuse.class.getClassLoader();
		InputStream is = classLoader.getResourceAsStream(file);
		ZipInputStream zis = new ZipInputStream(is);
	    
		ZipEntry ze = zis.getNextEntry();
		
	    InputStreamReader isr = new InputStreamReader(zis, Charset.forName("UTF-8"));
	    BufferedReader br = new BufferedReader(isr);
	    	
	    StreamEventPropagator sel = null;
		try {
			sel = SetupFactory.setupSimulation(tm, br);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return sel;
	}
	
}
