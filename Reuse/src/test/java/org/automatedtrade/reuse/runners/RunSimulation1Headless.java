package org.automatedtrade.reuse.runners;

import org.automatedtrade.reuse.struct.StreamEventPropagator;
import org.automatedtrade.reuse.struct.TimeSystem;

public class RunSimulation1Headless {

	public static void main(String args[]) {
		StreamEventPropagator sel = RunnerReuse.getPropagatorFromFileResource(new TimeSystem(), "1.trade");
		RunnerReuse.infiniteWait();		
	}

}
