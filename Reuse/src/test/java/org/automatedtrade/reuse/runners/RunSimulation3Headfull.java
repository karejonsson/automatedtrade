package org.automatedtrade.reuse.runners;

import java.io.IOException;

import org.automatedtrade.reuse.show.DurationVariableManagement;
import org.automatedtrade.reuse.show.DurationVariableManagementHeadfull;
import org.automatedtrade.reuse.show.MomentaryVariableManagement;
import org.automatedtrade.reuse.show.MomentaryVariableManagementHeadfull;
import org.automatedtrade.reuse.show.SignalProcessingForVariables;
import org.automatedtrade.reuse.struct.StreamEventPropagator;
import org.automatedtrade.reuse.struct.TimeManager;
import org.automatedtrade.reuse.struct.TimeSimulationFast;

public class RunSimulation3Headfull {

	public static void main(String args[]) throws IOException {
		TimeManager tm = new TimeSimulationFast();
		StreamEventPropagator sel = RunnerReuse.getPropagatorFromZippedFileResource(tm, "20170517_1900_4h.longtrade.zip");

		DurationVariableManagement[] gms = new DurationVariableManagement[3];
		gms[0] = new DurationVariableManagementHeadfull(10*60*1000, 10*60*10, "Ten minute graphs");
		gms[1] = new DurationVariableManagementHeadfull(60*60*1000, 60*60*10, "One hour graphs");
		gms[2] = new DurationVariableManagementHeadfull(4*60*60*1000, 4*60*60*10, "Four hour graphs");
		MomentaryVariableManagement mgm = new MomentaryVariableManagementHeadfull("Momentary bid/ask situation");

		SignalProcessingForVariables vop = new SignalProcessingForVariables(tm, gms, mgm);
		sel.addOrderBooksListener(vop);
		sel.addTradeListener(vop);

		RunnerReuse.infiniteWait();	

	}

}
