package org.automatedtrade.reuse.runners;

import org.automatedtrade.reuse.properties.CommandLineProperties;
import org.automatedtrade.reuse.properties.PropertiesAssemble;
import org.automatedtrade.reuse.show.DurationVariableManagement;
import org.automatedtrade.reuse.show.DurationVariableManagementHeadfull;
import org.automatedtrade.reuse.show.MomentaryVariableManagement;
import org.automatedtrade.reuse.show.MomentaryVariableManagementHeadfull;
import org.automatedtrade.reuse.show.SignalProcessingForVariables;
import org.automatedtrade.reuse.struct.StreamEventPropagator;
import org.automatedtrade.reuse.struct.TimeSystem;

public class RunBitstampLiveHeadfull {
	
    public static void main(final String[] args) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}
	
	public static void main(final CommandLineProperties clp) throws Exception {
		clp.printEvaluated();
		String apiKey = clp.getString("apikey", "de504dc5763aeef9ff52");
		StreamEventPropagator sel = RunnerReuse.getPropagatorFromBitstamp(apiKey);

		DurationVariableManagement[] gms = new DurationVariableManagement[3];
		gms[0] = new DurationVariableManagementHeadfull(10*60*1000, 10*60*10, "Ten minute graphs");
		gms[1] = new DurationVariableManagementHeadfull(60*60*1000, 60*60*10, "One hour graphs");
		gms[2] = new DurationVariableManagementHeadfull(4*60*60*1000, 4*60*60*10, "Four hour graphs");
		MomentaryVariableManagement mgm = new MomentaryVariableManagementHeadfull("Momentary bid/ask situation");

		SignalProcessingForVariables vop = new SignalProcessingForVariables(new TimeSystem(), gms, mgm);
		sel.addOrderBooksListener(vop);
		sel.addTradeListener(vop);
		
		RunnerReuse.infiniteWait();		
	}

}
