package org.automatedtrade.reuse.runners;

import java.io.IOException;

import org.automatedtrade.reuse.show.DurationVariableManagement;
import org.automatedtrade.reuse.show.DurationVariableManagementHeadless;
import org.automatedtrade.reuse.show.MomentaryVariableManagement;
import org.automatedtrade.reuse.show.MomentaryVariableManagementHeadless;
import org.automatedtrade.reuse.show.SignalProcessingForVariables;
import org.automatedtrade.reuse.struct.StreamEventPropagator;
import org.automatedtrade.reuse.struct.TimeManager;
import org.automatedtrade.reuse.struct.TimeSimulationFast;

public class RunSimulation3Headless {

	public static void main(String args[]) throws IOException {
		TimeManager tm = new TimeSimulationFast();
		StreamEventPropagator sel = RunnerReuse.getPropagatorFromZippedFileResource(tm, "20170517_1900_4h.longtrade.zip");

		DurationVariableManagement[] gms = new DurationVariableManagement[3];
		gms[0] = new DurationVariableManagementHeadless(10*60*1000, 10*60*10);
		gms[1] = new DurationVariableManagementHeadless(60*60*1000, 60*60*10);
		gms[2] = new DurationVariableManagementHeadless(4*60*60*1000, 4*60*60*10);
		MomentaryVariableManagement mgm = new MomentaryVariableManagementHeadless();

		SignalProcessingForVariables vop = new SignalProcessingForVariables(tm, gms, mgm);
		sel.addOrderBooksListener(vop);
		sel.addTradeListener(vop);

		RunnerReuse.infiniteWait();	

	}

}
