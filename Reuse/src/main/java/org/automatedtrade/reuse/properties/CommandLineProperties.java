package org.automatedtrade.reuse.properties;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

public class CommandLineProperties {
	
	private Properties props = null;
	private Map<String, String> user = new HashMap<String, String>();
	
  	public CommandLineProperties(Properties props) {
  		this.props = props;
  	}
  	
  	public Properties getPropertyObject() {
  		return props;
  	}
  	
  	public void setUserValue(String property, String value) {
  		user.put(property, value);
  	}
  	
  	public String getUserValue(String property) {
  		return user.get(property);
  	}
  	
  	public void clearUserValues() {
  		user.clear();
  	}
  	
  	public String getString(String pname) throws Exception {
  		String out = evaluateName(pname);
  		return out == null ? null : out.trim();
  	}
  	
  	public String getString(String pname, String _default) {
  		try {
  			String out = evaluateName(pname);
  			return out == null ? _default : out.trim();
  		}
  		catch(Exception e) {
  			return _default;
  		}
  	}
  	
  	public int getInt(String pname) throws Exception {
  		return Integer.parseInt(evaluateName(pname).trim());
  	}
  	
  	public Integer getInt_Immutable(String pname) throws Exception {
  		return Integer.parseInt(evaluateName(pname).trim());
  	}
  	
  	public int getInt(String pname, int _default) {
  		try {
  			String out = evaluateName(pname);
  			return out == null ? _default : Integer.parseInt(out.trim());
  		}
  		catch(Exception e) {
  			return _default;
  		}
  	}
  	
  	public Integer getInt_Immutable(String pname, Integer _default) {
  		try {
  			String out = evaluateName(pname);
  			return out == null ? _default : Integer.parseInt(out.trim());
  		}
  		catch(Exception e) {
  			return _default;
  		}
  	}
  	
  	public double getDouble(String pname) throws Exception {
  		return Double.parseDouble(evaluateName(pname).trim());
  	}
  	
  	public Double getDouble_Immutable(String pname) throws Exception {
  		return Double.parseDouble(evaluateName(pname).trim());
  	}
  	
  	public double getDouble(String pname, double _default) {
  		try {
  			String out = evaluateName(pname);
  			return out == null ? _default : Double.parseDouble(out.trim());
  		}
  		catch(Exception e) {
  			return _default;
  		}
  	}
  	
  	public Double getDouble_Immutable(String pname, Double _default) {
  		try {
  			String out = evaluateName(pname);
  			return out == null ? _default : Double.parseDouble(out.trim());
  		}
  		catch(Exception e) {
  			return _default;
  		}
  	}
  	
  	public long getLong(String pname) throws Exception {
  		return Long.parseLong(evaluateName(pname).trim());
  	}
  	
  	public Long getLong_Immutable(String pname) throws Exception {
  		return Long.parseLong(evaluateName(pname).trim());
  	}
  	
  	public long getLong(String pname, long _default) {
  		try {
  			String out = evaluateName(pname);
  			return out == null ? _default : Long.parseLong(out.trim());
  		}
  		catch(Exception e) {
  			return _default;
  		}
  	}
  	
  	public Long getLong_Immutable(String pname, Long _default) {
  		try {
  			String out = evaluateName(pname);
  			return out == null ? _default : Long.parseLong(out.trim());
  		}
  		catch(Exception e) {
  			return _default;
  		}
  	}
  	
  	public boolean getBoolean(String pname) throws Exception {
  		return Boolean.parseBoolean(evaluateName(pname).trim());
  	}
  	
  	public Boolean getBoolean_Immutable(String pname) throws Exception {
  		return Boolean.parseBoolean(evaluateName(pname).trim());
  	}
  	
  	public boolean getBoolean(String pname, boolean _default) {
  		try {
  			String out = evaluateName(pname);
  			return out == null ? _default : Boolean.parseBoolean(out.trim());
  		}
  		catch(Exception e) {
  			return _default;
  		}
  	}
  	
  	public Boolean getBoolean_Immutable(String pname, Boolean _default) {
  		try {
  			String out = evaluateName(pname);
  			return out == null ? _default : Boolean.parseBoolean(out.trim());
  		}
  		catch(Exception e) {
  			return _default;
  		}
  	}
  	
  	public Set<String> getNames() {
  		Set<String> out = new HashSet<String>();
  		for(Object o : props.keySet()) {
  			out.add(o.toString());
  		}
  		for(Object o : user.keySet()) {
  			out.add(o.toString());
  		}
  		
  		return out;
  	}
  	
  	private String evaluateName(String pname) throws Exception {
  		if(pname == null) {
  			return null;
  		}
  		//System.out.println("Evaluate name "+pname);
  		String val = props.getProperty(pname.trim());
  		if(val == null) {
  			String userVal = user.get(pname);
  			if(userVal != null) {
  				val = userVal;
  			}
  			else {
  	  			String msg = "Property value for name "+pname+" is missing.";
  	  			//System.err.println(msg);
  	  			throw new Exception(msg);
  			}
  		}
  		if(val.contains("${")) {
  			return evaluateValue(val);
  		}
  		return val;
  	}
  	
  	private String evaluateValue(String pvalue) throws Exception {
  		//System.out.println("Evaluate value "+pvalue);
  		try {
  	  		String val = pvalue;
  	  		while(val.contains("${")) {
  	  			String pre = val.substring(0, val.indexOf("${"));
  	  			String post = val.substring(val.indexOf("${")+2);
  	  			String variable = post.substring(0, post.indexOf("}"));
  	  			String replacement = evaluateName(variable);
  	  			val = pre+replacement+post.substring(post.indexOf("}")+1);
  	  		}
  	  		return val;
  		}
  		catch(Exception e) {
  			String msg = "Unable to evaluate value \""+pvalue+"\". Preceeding problem is:\n"+e.getMessage();
  			System.out.println(msg);
  			throw new Exception(msg);
  		}
  	}
  	
  	public void printRaw() {
  		PropertiesAssemble.print(props);
		for(String key : user.keySet()) {
			System.out.println(""+key+" = "+user.get(key));
		}
  	}
  	
  	public void printEvaluated() throws Exception {
  		Vector<String> list = new Vector<>();
		for(Object key : props.keySet()) {
			
			String a[] = null;
			try {
				a = toStringArray(key.toString());
			}
			catch(Exception e) {
				System.out.println(""+key+" could not be evaluated: "+e.getMessage());
				continue;
			}
			String keyLowered = (""+key).toLowerCase();
			String out = null;
			if(keyLowered.contains("pwd") || keyLowered.contains("passwd") || keyLowered.contains("password")) {
				out = ""+key+" = "+"<possible password not printed>";
			}
			else {
				if(a.length == 1) {
					out = ""+key+" = "+a[0];
				}
				else {
					out = ""+key+" = "+toString(a);
				}
			}
			//System.out.println(out);
			list.add(out);
		}
		for(String key : user.keySet()) {
			String keyLowered = (""+key).toLowerCase();
			try {
				String out = null;
				if(keyLowered.contains("pwd") || keyLowered.contains("passwd") || keyLowered.contains("password")) {
					out = ""+key+" = "+"<possible password not printed>";
				}
				else {
					out = ""+key+" = "+evaluateValue(user.get(key));
				}
				//System.out.println(out);
				list.add(out);
			}
			catch(Exception e) {
				System.err.println(""+key+" could not be evaluated: "+e.getMessage());
				continue;
			}
		}
		Collections.sort(list);
		for(int i=0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
  	}
  	
	public List<String> toStringList(String name) throws Exception {
		List<String> out = new ArrayList<String>();
		Object o = props.get(name);
		if(o == null) {
			String userVal = user.get(name);
			if(userVal != null) {
				out.add(evaluateValue(userVal));
			}
			return out;
		}
		if(o instanceof String) {
			out.add(evaluateValue((String) o));
			return out;
		}
		if(o instanceof Vector) {
			Vector<String> v = (Vector<String>) o;
			for(int i = 0 ; i < v.size() ; i++) {
				out.add(evaluateValue(v.elementAt(i)));
			}
		}
		return out;
	}


	public String[] toStringArray(String name) throws Exception {
		Object o = props.get(name);
		if(o == null) {
			String userVal = user.get(name);
			if(userVal != null) {
				return new String[] { evaluateValue(userVal) };
			}
			return new String[0];
		}
		if(o instanceof String) {
			return new String[] { (evaluateValue((String) o)) };
		}
		if(o instanceof Vector) {
			Vector<String> v = (Vector<String>) o;
			String out[] = new String[v.size()];
			for(int i = 0 ; i < out.length ; i++) {
				out[i] = evaluateValue(v.elementAt(i));
			}
			return out;
		}
		return new String[0];
	}

	public static String toString(String [] args) {
		if(args == null) {
			return "<NULL>";
		}
		if(args.length == 0) {
			return "[]";
		}
		if(args.length == 1) {
			return "["+args[0]+"]";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("["+args[0]);
		for(int i = 1 ; i < args.length ; i++) {
			sb.append(", "+args[i]);
		}
		return sb.toString()+"]";
	}
	
	public static void main(String args[]) throws Exception {
		CommandLineProperties clp = PropertiesAssemble.collectCLP(args);
		System.out.println("---------------- Rå form");
		clp.printRaw();
		System.out.println("---------------- Evaluerade");
		clp.printEvaluated();
	}

}