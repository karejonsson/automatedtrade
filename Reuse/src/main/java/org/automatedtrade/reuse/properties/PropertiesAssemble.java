package org.automatedtrade.reuse.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Vector;

public class PropertiesAssemble {

	public static final String separatorSequence = "separator";

	public static void main(String args[]) throws FileNotFoundException, IOException {
		
		Properties props = collect(args);
		print(props);
	}
	
	public static Properties collect(String args[]) throws FileNotFoundException, IOException {
		String propname = null;
		Properties props = new Properties();
		for(int i = 0 ; i < args.length ; i++) {
			if(args[i].startsWith("---")) {
				String propfilename = args[i].substring(3);
				Properties moreprops = new Properties();
				moreprops.load(new FileInputStream(propfilename+".properties"));
				moreprops = handleForSeparations(moreprops);
				for(Object key : moreprops.keySet()) {
					addSomePropertyValues(key.toString(), props, moreprops.get(key));
				}
				propname = null;
				continue;
			}
			if(args[i].startsWith("--")) {
				propname = args[i].substring(2);
				continue;
			}
			if(args[i].startsWith("-")) {
				propname = args[i].substring(1);
				continue;
			}
			addOnePropertyValue(propname, props, args[i]);
		}
		return props;
	}

	public static CommandLineProperties collectFromFile(String propfilename) throws FileNotFoundException, IOException {
		//System.out.println("PropertiesAssemble.collectFromFile - Anrop "+propfilename);
		File f = new File(propfilename);
		if(!f.exists()) {
			f = new File(propfilename+".properties");
			if(!f.exists()) {
				System.err.println("PropertiesAssemble.collectFromFile - Kom ingen vart med "+propfilename);
				return null;
			}
		}
		//System.out.println("PropertiesAssemble.collectFromFile - Använder "+f.getName());
		Properties props = new Properties();
		Properties moreprops = new Properties();
		moreprops.load(new FileInputStream(f));
		moreprops = handleForSeparations(moreprops);
		for(Object key : moreprops.keySet()) {
			addSomePropertyValues(key.toString(), props, moreprops.get(key));
		}
		return new CommandLineProperties(props);
	}
	
	public static Properties handleForSeparations(Properties in) {
		String separator = in.getProperty(separatorSequence);
		if(separator == null) {
			return in;
		}
		separator = separator.trim();
		if(separator.length() == 0) {
			return in;
		}
		Properties out = new Properties();
		for(Object key : in.keySet()) {
			String val = in.getProperty(key.toString());
			if(val.contains(separator)) {
				String pieces[] = val.split(separator);
				if(pieces.length == 1) {
					out.put(key, pieces[0].trim());
				}
				else {
					Vector<String> vals = new Vector<String>();
					for(int i = 0 ; i < pieces.length ; i++) {
						vals.add(pieces[i].trim());
					}
					out.put(key, vals);
				}
			}
			else {
				out.put(key, val);
			}
		}
		return out;
	}

	public static void addSomePropertyValues(String propname, Properties props, Object newValues) {
		//System.out.println("addSomePropertyValues propname="+propname+", newValues="+newValues);
		if(newValues instanceof String) {
			addOnePropertyValue(propname, props, (String) newValues);
			return;
		}
		if(newValues instanceof Vector) {
			Vector<String> v = (Vector<String>) newValues;
			for(int i = 0 ; i < v.size() ; i++) {
				addOnePropertyValue(propname, props, v.elementAt(i));
			}
			return;
		}
		//System.out.println("addSomePropertyValues class="+newValues.getClass().getName());
	}
	
	public static void addOnePropertyValue(String propname, Properties props, String newValue) {
		Object o = null;
		try {
			o = props.get(propname);
		}
		catch(Exception e) {
			System.err.println("Property <"+propname+"> was not available.");
			throw e;
		}
		if(o == null) {
			props.put(propname, newValue);
			return;
		}
		if(o instanceof String) {
			Vector<String> p = new Vector<String>();
			p.add((String) o);
			p.add(newValue);
			props.put(propname, p);
			return;
		}
		if(o instanceof Vector) {
			Vector<String> p = (Vector<String>) o;
			p.add(newValue);
			return;
		}
	}
	
	public static String[] toArray(Properties props, String name) {
		Object o = props.get(name);
		if(o == null) {
			return new String[0];
		}
		if(o instanceof String) {
			return new String[] { ((String) o) };
		}
		if(o instanceof Vector) {
			Vector<String> v = (Vector<String>) o;
			String out[] = new String[v.size()];
			for(int i = 0 ; i < out.length ; i++) {
				out[i] = v.elementAt(i);
			}
			return out;
		}
		return new String[0];
	}
	
	public static String toString(String [] args) {
		if(args == null) {
			return "<NULL>";
		}
		if(args.length == 0) {
			return "[]";
		}
		if(args.length == 1) {
			return "["+args[0]+"]";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("["+args[0]);
		for(int i = 1 ; i < args.length ; i++) {
			sb.append(", "+args[i]);
		}
		return sb.toString()+"]";
	}
	
	public static String toString(Vector<String> args) {
		if(args == null) {
			return "<NULL>";
		}
		if(args.size() == 0) {
			return "[]";
		}
		if(args.size() == 1) {
			return "["+args.elementAt(0)+"]";
		}
		StringBuilder sb = new StringBuilder();
		sb.append("["+args.elementAt(0));
		for(int i = 1 ; i < args.size() ; i++) {
			sb.append(", "+args.elementAt(i));
		}
		return sb.toString()+"]";
	}
	
	public static void print(Properties props) {
		for(Object key : props.keySet()) {
			String a[] = toArray(props, key.toString());
			if(a.length == 1) {
				System.out.println(""+key+" = "+a[0]);
			}
			else {
				System.out.println(""+key+" = "+toString(a));
			}
		}
	}

	public static CommandLineProperties collectCLP(String args[]) throws FileNotFoundException, IOException {
		Properties props = collect(args);
		return new CommandLineProperties(props);
	}

}