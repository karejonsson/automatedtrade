package org.automatedtrade.reuse.show;

import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JFrame;

import org.automatedtrade.reuse.plot.PlotCoordinate;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class MomentaryVariableManagementHeadfull extends MomentaryVariableManagement {

	private boolean multiWindowLingering = true;
	private static final int h = 300;
	private static final int w = 600;
	private PanelWithClosePlaces pwfp = new PanelWithClosePlaces(1, 2);	
	private JFrame comboFrame = null;

	private String title;
	
	public MomentaryVariableManagementHeadfull(String title) {
		this.title = title;
	}

	private void setupMultiWindow() {
		comboFrame = new JFrame(title);
		comboFrame.getContentPane().add(pwfp);
        comboFrame.pack();
        comboFrame.setVisible(true);
		comboFrame.setSize(new Dimension(w, h));
	}

	protected void visualizeOrderBookBoundaries(Vector<PlotCoordinate> bidsV, Vector<PlotCoordinate> asksV) {
		if(multiWindowLingering) {
			setupMultiWindow();
			multiWindowLingering = false;
		}

		final XYSeries bids = new XYSeries("Bids");
        for(int i = 0 ; i < bidsV.size() ; i++) {
        	PlotCoordinate pc = bidsV.get(i);
        	bids.add(pc.horizontal, pc.vertical);
        }
        
        final XYSeries asks = new XYSeries("Asks");
        for(int i = 0 ; i < asksV.size() ; i++) {
        	PlotCoordinate pc = asksV.get(i);
        	asks.add(pc.horizontal, pc.vertical);
        }
        
        final XYSeriesCollection data = new XYSeriesCollection();
        data.addSeries(bids);
        data.addSeries(asks);
        final JFreeChart chart = ChartFactory.createXYLineChart(
            "All bids and asks",
            "Price", 
            "Accumulated amount", 
            data,
            PlotOrientation.VERTICAL,
            true,
            true,
            false
        );

        final ChartPanel chartPanel = new ChartPanel(chart);
        pwfp.setPanel(0, 0, chartPanel); 
	}

	protected void visualizeAskBidSubset(Vector<PlotCoordinate> subsetBids, Vector<PlotCoordinate> subsetAsks) {
		if(multiWindowLingering) {
			setupMultiWindow();
			multiWindowLingering = false;
		}
        final XYSeries bids = new XYSeries("Bids");
        for(int i = 0 ; i < subsetBids.size() ; i++) {
        	PlotCoordinate pc = subsetBids.get(i);
        	bids.add(pc.horizontal, pc.vertical);
        }
        
        final XYSeries asks = new XYSeries("Asks");
        for(int i = 0 ; i < subsetAsks.size() ; i++) {
        	PlotCoordinate pc = subsetAsks.get(i);
        	asks.add(pc.horizontal, pc.vertical);
        }
        
        final XYSeriesCollection data = new XYSeriesCollection();
        data.addSeries(bids);
        data.addSeries(asks);
        final JFreeChart chart = ChartFactory.createXYLineChart(
            "Subset close to gap",
            "Price", 
            "Accumulated amount", 
            data,
            PlotOrientation.VERTICAL,
            true,
            true,
            false
        );

        final ChartPanel chartPanel = new ChartPanel(chart);
        pwfp.setPanel(0, 1, chartPanel); 
    }

}
