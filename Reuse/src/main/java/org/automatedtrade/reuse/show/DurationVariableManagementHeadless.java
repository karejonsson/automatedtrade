package org.automatedtrade.reuse.show;

import java.util.Vector;

import org.automatedtrade.reuse.plot.PlotCoordinate;

public class DurationVariableManagementHeadless extends DurationVariableManagement {

	public DurationVariableManagementHeadless(long millisInGraph, long millisSampling) {
		super(millisInGraph, millisSampling);
	}

	@Override
	protected void visualizeAskBidLeanDirection(Vector<PlotCoordinate> subsetBids, Vector<PlotCoordinate> subsetAsks,
			long timeMillis) {
	}

	@Override
	public void visualizeAmount(double amount, long timeMillis) {
	}

	@Override
	public void visualizeBoundaries(Vector<PlotCoordinate> bids, Vector<PlotCoordinate> asks, long timeMillis) {
	}

	@Override
	public void visualizeTradeInGap(Vector<PlotCoordinate> bids, Vector<PlotCoordinate> asks) {
	}

	@Override
	public void visualizePrice(double price, long timeMillis) {
	}

	@Override
	public void visualizePriceVariation(long timeMillis) {
	}

	@Override
	public void visualizeTradingIntensity(long timeMillis) {
	}

}
