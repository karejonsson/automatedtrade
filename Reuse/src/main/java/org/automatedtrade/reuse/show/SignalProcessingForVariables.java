package org.automatedtrade.reuse.show;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.automatedtrade.reuse.plot.PlotCoordinate;
import org.automatedtrade.reuse.struct.StreamEventPropagator;
import org.automatedtrade.reuse.struct.TimeManager;

public class SignalProcessingForVariables implements StreamEventPropagator.OrderBooks, StreamEventPropagator.TradeListener {

	private DurationVariableManagement[] gms = null;
	private MomentaryVariableManagement mgm = null;
	private TimeManager to;
	
	public SignalProcessingForVariables(TimeManager to, DurationVariableManagement[] gms, MomentaryVariableManagement mgm) {
		this.to = to;
		this.gms = gms;
		this.mgm = mgm;
	}
	
	public void orderbook(Map<String, String> map) {
		List<List<String>> bidsFlat = (List<List<String>>) ((Object) map.get("bids"));
		
		double minbid = Double.MAX_VALUE;
		double maxbid = Double.MIN_VALUE;
		Double accum = 0.0;
		Vector<PlotCoordinate> bidsAccum = new Vector<PlotCoordinate>();
		for(List<String> b : bidsFlat) {
			double bid = Double.parseDouble(b.get(0));
			minbid = Math.min(minbid, bid);
			maxbid = Math.max(maxbid, bid);
			Double amount = Double.parseDouble(b.get(1));
			accum += amount;
			PlotCoordinate c = new PlotCoordinate();
			c.vertical = accum;
			c.horizontal = bid;
			bidsAccum.add(0, c);
			//System.out.println("Bid "+bid+", amount "+amount+", accum "+accum);
		}

		List<List<String>> asksFlat = (List<List<String>>) ((Object) map.get("asks"));
		double minask = Double.MAX_VALUE;
		double maxask = Double.MIN_VALUE;
		accum = 0.0;
		Vector<PlotCoordinate> asksAccum = new Vector<PlotCoordinate>();
		for(List<String> a : asksFlat) {
			double ask = Double.parseDouble(a.get(0));
			minask = Math.min(minask, ask);
			maxask = Math.max(maxask, ask);
			Double amount = Double.parseDouble(a.get(1));
			accum += amount;
			PlotCoordinate c = new PlotCoordinate();
			c.vertical = accum;
			c.horizontal = ask;
			asksAccum.add(c);
			//System.out.println("Ask "+ask+", amount "+amount+", accum "+accum);
		} 
		
		for(int i = 0 ;i < gms.length ; i++) {
			gms[i].boundaries(bidsAccum, asksAccum, to.getTime());
			gms[i].tradeInGap(bidsAccum, asksAccum);	
		}		
		mgm.orderBookBoundaries(bidsAccum, asksAccum);
		
    	final double fraction = 0.2;
    	
    	double bidHigh = bidsAccum.get(bidsAccum.size()-1).vertical;
    	double bidAmount = bidsAccum.get(0).vertical;
    	double limitBid = bidAmount*fraction+bidHigh;//bidHigh-fraction*(bidHigh-bidLow);
    	Vector<PlotCoordinate> subsetBids = new Vector<PlotCoordinate>();
    	for(int idx = bidsAccum.size()-1 ; ; idx--) {
    		PlotCoordinate pc = bidsAccum.get(idx);
    		if(pc.vertical < limitBid) {
    			subsetBids.add(0, pc);
    		}
    		else {
    			break;
    		}
    	}
    	
    	double askLow = asksAccum.get(0).vertical;
    	double askHigh = asksAccum.get(asksAccum.size()-1).vertical;
    	double limitAsk = askHigh*fraction+askLow;//askLow+fraction*(askHigh-askLow);
    	Vector<PlotCoordinate> subsetAsks = new Vector<PlotCoordinate>();
    	for(int idx = 0 ; ; idx++) {
    		PlotCoordinate pc = asksAccum.get(idx);
    		if(pc.vertical < limitAsk) {
    			subsetAsks.add(pc);
    		}
    		else {
    			break;
    		}
    	}
    	
		for(int i = 0 ;i < gms.length ; i++) {
	      	gms[i].askBidLeanDirection(subsetBids, subsetAsks, to.getTime());
		}
		mgm.askBidSubset(subsetBids, subsetAsks);
	}
		
	@Override
	public void trade(Map<String, String> map) {
		//System.out.println("Trade "+map);
		double price = ((Double) ((Object) map.get("price")));
		double amount = ((Double) ((Object) map.get("amount")));
		
		for(int i = 0 ;i < gms.length ; i++) {
			gms[i].price(price, to.getTime());
			gms[i].priceVariation(to.getTime());
			gms[i].amount(amount, to.getTime());
			gms[i].tradingIntensity(to.getTime());		
		}
	}
   
    public void difforderBook(Map<String, String> map) {
	}

}
