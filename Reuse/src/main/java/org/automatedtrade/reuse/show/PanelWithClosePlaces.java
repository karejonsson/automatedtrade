package org.automatedtrade.reuse.show;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class PanelWithClosePlaces extends JPanel {
	
	public void setPanel(int v, int h, JComponent c) {
		panels[v][h].removeAll();
		panels[v][h].add(c);
		c.setMinimumSize(c.getSize());
		panels[v][h].repaint();
		c.repaint();
		invalidate();
		validate();
		repaint();
	}
	
	//private final static double w = 0.333;
	//private final static double h = 0.5;
	
	private JPanel panels[][] = null;

	public PanelWithClosePlaces(int vertically, int horizontally) {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		removeAll();
		
		panels = new JPanel[vertically][];
		for(int v = 0 ; v < vertically ; v++) {
			panels[v] = new JPanel[horizontally];
			JPanel ver = new PVer(1.0/vertically);
			ver.setLayout(new BoxLayout(ver, BoxLayout.LINE_AXIS));
			for(int h = 0 ; h < horizontally ; h++) {
				JPanel tmp = new PHor(1.0/horizontally);
				ver.add(tmp);
				panels[v][h] = tmp;
			}
			add(ver);
		}
	}

	private static class PHor extends JPanel {
		private double fraction;
		public PHor(double fraction) {
			this.fraction = fraction;
			setLayout(new BorderLayout(2,2));
		}
		
		@Override
		public Dimension getSize() {
			return getPreferredSize();
		}

		@Override
		public Dimension getPreferredSize() {
			Dimension d = getParent().getSize();
			int w = (int) (d.width * fraction);
			//System.out.println("VER w "+w);
			return new Dimension(w, d.height);
		}
	}

	private class PVer extends JPanel {
		private double fraction;
		public PVer(double fraction) {
			this.fraction = fraction;
			setLayout(new BorderLayout(2,2));
		}

		@Override
		public Dimension getSize() {
			return getPreferredSize();
		}

		@Override
		public Dimension getPreferredSize() {
			Dimension d = PanelWithClosePlaces.this.getParent().getParent().getSize();
			int h = (int) (d.height * fraction);
			//System.out.println("HOR h "+h);
			return new Dimension(d.width, h);
		}
	}

}
