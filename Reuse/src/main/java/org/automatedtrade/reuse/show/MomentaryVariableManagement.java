package org.automatedtrade.reuse.show;

import java.awt.Dimension;
import java.util.Vector;

import javax.swing.JFrame;

import org.automatedtrade.reuse.plot.PlotCoordinate;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public abstract class MomentaryVariableManagement {

	protected abstract void visualizeOrderBookBoundaries(Vector<PlotCoordinate> bidsV, Vector<PlotCoordinate> asksV);
	
	public void orderBookBoundaries(Vector<PlotCoordinate> bidsV, Vector<PlotCoordinate> asksV) {
		visualizeOrderBookBoundaries(bidsV, asksV);
	}

	protected abstract void visualizeAskBidSubset(Vector<PlotCoordinate> subsetBids, Vector<PlotCoordinate> subsetAsks);
    
    public void askBidSubset(Vector<PlotCoordinate> subsetBids, Vector<PlotCoordinate> subsetAsks) {
    	visualizeAskBidSubset(subsetBids, subsetAsks);
    }

}
