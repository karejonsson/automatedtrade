package org.automatedtrade.reuse.show;

import java.util.Vector;

import org.automatedtrade.reuse.plot.PlotCoordinate;

public class MomentaryVariableManagementHeadless extends MomentaryVariableManagement {

	@Override
	protected void visualizeOrderBookBoundaries(Vector<PlotCoordinate> bidsV, Vector<PlotCoordinate> asksV) {
	}

	@Override
	protected void visualizeAskBidSubset(Vector<PlotCoordinate> subsetBids, Vector<PlotCoordinate> subsetAsks) {
	}

}
