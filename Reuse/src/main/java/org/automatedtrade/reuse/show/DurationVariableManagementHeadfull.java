package org.automatedtrade.reuse.show;

import java.awt.Dimension;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.swing.JFrame;

import org.automatedtrade.reuse.plot.PlotCoordinate;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.time.TimeSeriesCollection;

public class DurationVariableManagementHeadfull extends DurationVariableManagement {

	private static final int h = 600;
	private static final int w = 900;
	
	private boolean multiWindowLingering = true;

	private JFrame comboFrame = null;
	private PanelWithClosePlaces pwfp = null;
	private String title;

	public DurationVariableManagementHeadfull(long millisInGraph, long millisSampling, String title) {
		super(millisInGraph, millisSampling);
		this.title = title;
	}

	private void setupMultiWindow() {
		pwfp = new PanelWithClosePlaces(2, 4);
		comboFrame = new JFrame(title);
		comboFrame.getContentPane().add(pwfp);
        comboFrame.pack();
        comboFrame.setVisible(true);
		comboFrame.setSize(new Dimension(w, h));
	}

	private TimeSeriesCollection leaningdata = new TimeSeriesCollection();	
	private TimeSeriesCollection tippingdata = new TimeSeriesCollection();
	private boolean leaningNotSeen = true;
	
	protected void visualizeAskBidLeanDirection(Vector<PlotCoordinate> subsetBids, Vector<PlotCoordinate> subsetAsks, long timeMillis) {
		if(leaningNotSeen) {
			if(multiWindowLingering) {
				setupMultiWindow();
				multiWindowLingering = false;
			}
			leaningdata.addSeries(leaning);
	    	
	        JFreeChart leanchart = ChartFactory.createTimeSeriesChart(
	                "Tendency > 1 -> Up", 
	                "", // Text below chart
	                "leaning", 
	                leaningdata,
	                true,
	                true,
	                false
	        );
	        final XYPlot plot = leanchart.getXYPlot();
	        DateAxis domainaxis = (DateAxis) plot.getDomainAxis();
	        domainaxis.setAutoRange(true);
	        domainaxis.setFixedAutoRange(millisInGraph);
	        domainaxis.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
	        NumberAxis valueaxis = (NumberAxis) plot.getRangeAxis();
	        valueaxis.setAutoRangeIncludesZero(false);

	        ChartPanel leanlabel = new ChartPanel(leanchart);
	        pwfp.setPanel(1, 2, leanlabel);

			tippingdata.addSeries(tipping);

			JFreeChart tipchart = ChartFactory.createTimeSeriesChart(
	                "Tipping", 
	                "", // Text below chart
	                "leaning", 
	                tippingdata,
	                true,
	                true,
	                false
	        );
	        final XYPlot tipplot = tipchart.getXYPlot();
	        DateAxis tipdomainaxis = (DateAxis) tipplot.getDomainAxis();
	        tipdomainaxis.setAutoRange(true);
	        tipdomainaxis.setFixedAutoRange(millisInGraph);
	        tipdomainaxis.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
	        NumberAxis tipvalueaxis = (NumberAxis) tipplot.getRangeAxis();
	        tipvalueaxis.setAutoRangeIncludesZero(false);

	        ChartPanel tiplabel = new ChartPanel(tipchart);
	        pwfp.setPanel(0, 2, tiplabel);

	        leaningNotSeen = false;
		}
    }
	
	private TimeSeriesCollection tradedata = new TimeSeriesCollection();
	private boolean amountNotSeen = true;

	public void visualizeAmount(double amount, long timeMillis) {
		if(amountNotSeen) {
			if(multiWindowLingering) {
				setupMultiWindow();
				multiWindowLingering = false;
			}
			//tradedata.setType(HistogramType.RELATIVE_FREQUENCY);
			//tradedata.addSeries(tradeamount);
			tradedata.addSeries(tradeamount);
		 
			JFreeChart chart = ChartFactory.createXYBarChart(
					"Amounts traded", 
					"", // Text below chart
					true, 
					"Amount", 
					tradedata, 
					PlotOrientation.VERTICAL, 
					true, true, false);

	        final XYPlot plot = chart.getXYPlot();
	        DateAxis domainaxis = (DateAxis) plot.getDomainAxis();
	        domainaxis.setAutoRange(true);
	        domainaxis.setFixedAutoRange(millisInGraph);
	        domainaxis.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
	        NumberAxis valueaxis = (NumberAxis) plot.getRangeAxis();
	        //valueaxis.setAutoRangeIncludesZero(false);

	        ChartPanel label = new ChartPanel(chart);
	        pwfp.setPanel(1, 0, label);
	        amountNotSeen = false;
		}
	}

	private TimeSeriesCollection boundairesdata = new TimeSeriesCollection();
	private boolean boundariesNotSeen = true;

    public void visualizeBoundaries(Vector<PlotCoordinate> bids, Vector<PlotCoordinate> asks, long timeMillis) {		
		if(boundariesNotSeen) {
			if(multiWindowLingering) {
				setupMultiWindow();
				multiWindowLingering = false;
			}
	        
			boundairesdata.addSeries(boundaireslowestbid);
			boundairesdata.addSeries(highestbid);
			boundairesdata.addSeries(lowestask);
			boundairesdata.addSeries(boundaireshighestask);
	        
	        JFreeChart chart = ChartFactory.createTimeSeriesChart(
	                "Separation", 
	                "", // Text below chart
	                "Price", 
	                boundairesdata,
	                true,
	                true, 
	                false
	        );
	        
	        final XYPlot plot = chart.getXYPlot();
	        DateAxis domainaxis = (DateAxis) plot.getDomainAxis();
	        domainaxis.setAutoRange(true);
	        domainaxis.setFixedAutoRange(millisInGraph);
	        domainaxis.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
	        NumberAxis valueaxis = (NumberAxis) plot.getRangeAxis();
	        valueaxis.setAutoRangeIncludesZero(false);

	        ChartPanel label = new ChartPanel(chart);
	        pwfp.setPanel(0, 1, label);
			boundariesNotSeen = false;
		}
	}

	private TimeSeriesCollection tradeInGapdata = new TimeSeriesCollection();
	private boolean tradeInGapNotSeen = true;

    public void visualizeTradeInGap(Vector<PlotCoordinate> bids, Vector<PlotCoordinate> asks) {	
		if(tradeInGapNotSeen) {
			if(multiWindowLingering) {
				setupMultiWindow();
				multiWindowLingering = false;
			}
	        
			tradeInGapdata.addSeries(highestbid);
			tradeInGapdata.addSeries(lowestask);
			tradeInGapdata.addSeries(tradeprice);
	        
	        JFreeChart chart = ChartFactory.createTimeSeriesChart(
	                "Gap and trades", 
	                "", // Text below chart
	                "Price", 
	                tradeInGapdata,
	                true,
	                true,
	                false
	        );
	        
	        final XYPlot plot = chart.getXYPlot();
	        DateAxis domainaxis = (DateAxis) plot.getDomainAxis();
	        domainaxis.setAutoRange(true);
	        domainaxis.setFixedAutoRange(millisInGraph);
	        domainaxis.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
	        NumberAxis valueaxis = (NumberAxis) plot.getRangeAxis();
	        valueaxis.setAutoRangeIncludesZero(false);

	        ChartPanel label = new ChartPanel(chart);
	        pwfp.setPanel(1, 1, label);
	        tradeInGapNotSeen = false;
		}
	}

	private TimeSeriesCollection pricedata = new TimeSeriesCollection();
	private boolean priceNotSeen = true;
	private double lastPriceSeen;

	public void visualizePrice(double price, long timeMillis) {	
		if(priceNotSeen) {
			if(multiWindowLingering) {
				setupMultiWindow();
				multiWindowLingering = false;
			}
	         
			pricedata.addSeries(tradeprice);
	        
	        JFreeChart chart = ChartFactory.createTimeSeriesChart(
	                "Effectuated prices",
	                "", // Text below chart
	                "Price", 
	                pricedata,
	                true,
	                true,
	                false
	        );
	        
	        final XYPlot plot = chart.getXYPlot();
	        DateAxis domainaxis = (DateAxis) plot.getDomainAxis();
	        domainaxis.setAutoRange(true);
	        domainaxis.setFixedAutoRange(millisInGraph);
	        domainaxis.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
	        NumberAxis valueaxis = (NumberAxis) plot.getRangeAxis();
	        valueaxis.setAutoRangeIncludesZero(false);

	        ChartPanel label = new ChartPanel(chart);
	        pwfp.setPanel(0, 0, label);
			priceNotSeen = false;
		}
	}

	private TimeSeriesCollection pricevariationdata = new TimeSeriesCollection();
	private boolean variationNotSeen = true;
	
	public void visualizePriceVariation(long timeMillis) {		
		if(variationNotSeen) {
			if(multiWindowLingering) {
				setupMultiWindow();
				multiWindowLingering = false;
			}
	         
			pricevariationdata.addSeries(pricevariation);
	        
	        JFreeChart chart = ChartFactory.createTimeSeriesChart(
	                "Price variation",
	                "", // Text below chart
	                "Variation", 
	                pricevariationdata,
	                true,
	                true,
	                false
	        );
	        
	        final XYPlot plot = chart.getXYPlot();
	        DateAxis domainaxis = (DateAxis) plot.getDomainAxis();
	        domainaxis.setAutoRange(true);
	        domainaxis.setFixedAutoRange(millisInGraph);
	        domainaxis.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
	        NumberAxis valueaxis = (NumberAxis) plot.getRangeAxis();
	        valueaxis.setAutoRangeIncludesZero(false);

	        ChartPanel label = new ChartPanel(chart);
	        pwfp.setPanel(0, 3, label);
	        variationNotSeen = false;
		}
	}

	private TimeSeriesCollection tradinginsensitydata = new TimeSeriesCollection();
	private boolean intensityNotSeen = true;
	
	public void visualizeTradingIntensity(long timeMillis) {		
		if(intensityNotSeen) {
			if(multiWindowLingering) {
				setupMultiWindow();
				multiWindowLingering = false;
			}
	         
			tradinginsensitydata.addSeries(tradinginsensity);
	        
	        JFreeChart chart = ChartFactory.createTimeSeriesChart(
	                "Trading intensity",
	                "", // Text below chart
	                "Money", 
	                tradinginsensitydata,
	                true,
	                true,
	                false
	        );
	        
	        final XYPlot plot = chart.getXYPlot();
	        DateAxis domainaxis = (DateAxis) plot.getDomainAxis();
	        domainaxis.setAutoRange(true);
	        domainaxis.setFixedAutoRange(millisInGraph);
	        domainaxis.setDateFormatOverride(new SimpleDateFormat("HH:mm:ss"));
	        NumberAxis valueaxis = (NumberAxis) plot.getRangeAxis();
	        valueaxis.setAutoRangeIncludesZero(false);

	        ChartPanel label = new ChartPanel(chart);
	        pwfp.setPanel(1, 3, label);
	        intensityNotSeen = false;
		} 
	}

}
