package org.automatedtrade.reuse.show;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.Vector;

import org.apache.commons.math3.stat.descriptive.moment.Variance;
import org.automatedtrade.reuse.plot.PlotCoordinate;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesDataItem;

public abstract class DurationVariableManagement {
	
	private static final int millisInGraph_default = 600000;
	
	protected long millisInGraph;
	private long millisSampling;

	public DurationVariableManagement(long millisInGraph, long millisSampling) {
		this.millisInGraph = millisInGraph;
		this.millisSampling = millisSampling;
	}
	
	protected TimeSeries leaning = new TimeSeries("Bid/ask leaning", Second.class);
	protected TimeSeries tipping = new TimeSeries("Leaning tendency", Second.class);

	protected abstract void visualizeAskBidLeanDirection(Vector<PlotCoordinate> subsetBids, Vector<PlotCoordinate> subsetAsks, long timeMillis);

	public void askBidLeanDirection(Vector<PlotCoordinate> subsetBids, Vector<PlotCoordinate> subsetAsks, long timeMillis) {
		visualizeAskBidLeanDirection(subsetBids, subsetAsks, timeMillis);
		double uptendency = 
				((subsetAsks.get(subsetAsks.size()-1).vertical - subsetAsks.get(0).vertical) /
				 (subsetAsks.get(subsetAsks.size()-1).horizontal - subsetAsks.get(0).horizontal)) 
				/ 
				((subsetBids.get(0).vertical - subsetBids.get(subsetBids.size()-1).vertical) /
				 (subsetBids.get(subsetBids.size()-1).horizontal - subsetBids.get(0).horizontal));
		if(uptendency > 1) {
			uptendency = 1+(1.0-(1/uptendency));
		}
		
		double tippingtendency = uptendency/(subsetAsks.get(0).horizontal-subsetBids.get(subsetBids.size()-1).horizontal);
		
		try {
			leaning.addOrUpdate(new Second(new Date(timeMillis)), uptendency);
			leaning.removeAgedItems(timeMillis-millisInGraph, false);
			tipping.addOrUpdate(new Second(new Date(timeMillis)), tippingtendency);
			tipping.removeAgedItems(timeMillis-millisInGraph, false);
		}
		catch(Exception e) {
		}
    }

	protected TimeSeries tradeamount = new TimeSeries("Traded price", Second.class);

	public abstract void visualizeAmount(double amount, long timeMillis);
	
	public void amount(double amount, long timeMillis) {
		visualizeAmount(amount, timeMillis);
		try {
			tradeamount.addOrUpdate(new Second(new Date(timeMillis)), amount);
			tradeamount.removeAgedItems(timeMillis-millisInGraph, false);
		}
		catch(Exception e) {
		}
	}
	/*
		tradeamount.addOrUpdate(new Second(new Date(timeMillis)), amount);

Exception in thread "Thread-0" java.lang.IllegalArgumentException: Range(double, double): require lower (1.49504073E12) <= upper (1.494975329999E12).
	at org.jfree.data.Range.<init>(Range.java:90)
	at org.jfree.data.time.TimeSeriesCollection.getDomainBounds(TimeSeriesCollection.java:614)
	at org.jfree.data.general.DatasetUtilities.findDomainBounds(DatasetUtilities.java:666)
	at org.jfree.chart.renderer.xy.XYBarRenderer.findDomainBounds(XYBarRenderer.java:1182)
	at org.jfree.chart.plot.XYPlot.getDataRange(XYPlot.java:4456)
	at org.jfree.chart.axis.DateAxis.autoAdjustRange(DateAxis.java:1284)
	at org.jfree.chart.axis.DateAxis.configure(DateAxis.java:716)
	at org.jfree.chart.plot.XYPlot.configureDomainAxes(XYPlot.java:959)
	at org.jfree.chart.plot.XYPlot.datasetChanged(XYPlot.java:4513)
	at org.jfree.data.general.AbstractDataset.notifyListeners(AbstractDataset.java:185)
	at org.jfree.data.general.AbstractDataset.fireDatasetChanged(AbstractDataset.java:168)
	at org.jfree.data.general.AbstractSeriesDataset.seriesChanged(AbstractSeriesDataset.java:110)
	at org.jfree.data.general.Series.notifyListeners(Series.java:328)
	at org.jfree.data.general.Series.fireSeriesChanged(Series.java:313)
	at org.jfree.data.time.TimeSeries.addOrUpdate(TimeSeries.java:712)
	at org.jfree.data.time.TimeSeries.addOrUpdate(TimeSeries.java:662)
	at org.automatedtrade.reuse.show.DurationVariableManagement.visualizeAmount(DurationVariableManagement.java:167)
	at org.automatedtrade.reuse.show.SignalProcessingForVariables.trade(SignalProcessingForVariables.java:115)
	 */
	
	protected TimeSeries boundaireslowestbid = new TimeSeries("Lowest bid", Second.class);
	protected TimeSeries highestbid = new TimeSeries("Highest bid", Second.class);
	protected TimeSeries lowestask = new TimeSeries("Lowest ask", Second.class);
	protected TimeSeries boundaireshighestask = new TimeSeries("Highest ask", Second.class);

    protected abstract void visualizeBoundaries(Vector<PlotCoordinate> bids, Vector<PlotCoordinate> asks, long timeMillis);
    
    public void boundaries(Vector<PlotCoordinate> bids, Vector<PlotCoordinate> asks, long timeMillis) {
    	visualizeBoundaries(bids, asks, timeMillis);
		try {
			boundaireslowestbid.addOrUpdate(new Second(new Date(timeMillis)), bids.get(0).horizontal);
			boundaireslowestbid.removeAgedItems(timeMillis-millisInGraph, false);
			highestbid.addOrUpdate(new Second(new Date(timeMillis)), bids.get(bids.size()-1).horizontal);
			highestbid.removeAgedItems(timeMillis-millisInGraph, false);
			lowestask.addOrUpdate(new Second(new Date(timeMillis)), asks.get(0).horizontal);
			lowestask.removeAgedItems(timeMillis-millisInGraph, false);
			boundaireshighestask.addOrUpdate(new Second(new Date(timeMillis)), asks.get(bids.size()-1).horizontal);
			boundaireshighestask.removeAgedItems(timeMillis-millisInGraph, false);
		}
		catch(Exception e) {
		}
    }


    protected abstract void visualizeTradeInGap(Vector<PlotCoordinate> bids, Vector<PlotCoordinate> asks);
    
    public void tradeInGap(Vector<PlotCoordinate> bids, Vector<PlotCoordinate> asks) {	
    	visualizeTradeInGap(bids, asks);
	}

	protected TimeSeries tradeprice = new TimeSeries("Traded price", Second.class);
	
	private double lastPriceSeen;

	protected abstract void visualizePrice(double price, long timeMillis);
	
	public void price(double price, long timeMillis) {
		visualizePrice(price, timeMillis);
		lastPriceSeen = price;
		try {
			tradeprice.addOrUpdate(new Second(new Date(timeMillis)), price);
			tradeprice.removeAgedItems(timeMillis-millisInGraph, false);
		}
		catch(Exception e) {
		}
	}
	
	protected TimeSeries pricevariation = new TimeSeries("Price variation", Second.class);

	protected abstract void visualizePriceVariation(long timeMillis);
	
	public void priceVariation(long timeMillis) {
		visualizePriceVariation(timeMillis);
		try {
			pricevariation.addOrUpdate(new Second(new Date(timeMillis)), sampleForVariation(tradeprice, timeMillis));
			pricevariation.removeAgedItems(timeMillis-millisInGraph, false);
		}
		catch(Exception e) {
		}
	}
	
	protected TimeSeries tradinginsensity = new TimeSeries("Trading intensity", Second.class);
	
	protected abstract void visualizeTradingIntensity(long timeMillis);
	
	public void tradingIntensity(long timeMillis) {		
		visualizeTradingIntensity(timeMillis);
		try {
			tradinginsensity.addOrUpdate(new Second(new Date(timeMillis)), sampleForTradingIntensity(tradeamount, timeMillis));
			tradinginsensity.removeAgedItems(timeMillis-millisInGraph, false);
		}
		catch(Exception e) {
		}
	}
	
	private static TimeZone timeZone = TimeZone.getDefault();
	
	private Variance var = new Variance();
	
	private Double sampleForVariation(TimeSeries ts, long timeMillis) {
		RegularTimePeriod rtps = RegularTimePeriod.createInstance(Second.class, new Date(timeMillis-millisSampling), timeZone);
		RegularTimePeriod rtpe = RegularTimePeriod.createInstance(Second.class, new Date(timeMillis), timeZone);
		TimeSeries subset = null;
		try {
			subset = ts.createCopy(rtps, rtpe);
		} catch (CloneNotSupportedException e) {
			return null;
		}
		List<TimeSeriesDataItem> values = subset.getItems();
		double out[] = new double[values.size()];
		for(int i = 0 ; i < out.length ; i++) {
			out[i] = (double) values.get(i).getValue();
		}
		return var.evaluate(out);
	}
	
	private double sigmoidAttenuation(long sampleTimeMillis, long currentTimeMillis) {
		double arg = 4.0-(8.0*(currentTimeMillis-sampleTimeMillis))/millisSampling;
		double out = 1.0/(1 + Math.exp(-arg));
		//System.out.println("Time "+arg+", attenuation "+out);
		return out;
	}
 	
	// http://mathworld.wolfram.com/SigmoidFunction.html
	private Double sampleForTradingIntensity(TimeSeries ts, long timeMillis) {
		RegularTimePeriod rtps = RegularTimePeriod.createInstance(Second.class, new Date(timeMillis-millisSampling), timeZone);
		RegularTimePeriod rtpe = RegularTimePeriod.createInstance(Second.class, new Date(timeMillis), timeZone);
		TimeSeries subset = null;
		try {
			subset = ts.createCopy(rtps, rtpe);
		} catch (CloneNotSupportedException e) {
			return null;
		}
		List<TimeSeriesDataItem> values = subset.getItems();
		double out = 0;
		//System.out.println("-------------- millisSampling "+millisSampling);
		for(int i = 0 ; i < values.size() ; i++) {
			TimeSeriesDataItem tsdi = values.get(i);
			out += ((double) tsdi.getValue()) * sigmoidAttenuation(tsdi.getPeriod().getMiddleMillisecond(), timeMillis);
		}
		return out;
	}
 	
}
