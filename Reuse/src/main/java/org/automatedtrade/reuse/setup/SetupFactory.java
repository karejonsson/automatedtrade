package org.automatedtrade.reuse.setup;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import org.automatedtrade.reuse.properties.CommandLineProperties;
import org.automatedtrade.reuse.properties.PropertiesAssemble;
import org.automatedtrade.reuse.struct.StreamLiveFromBitstamp;
import org.automatedtrade.reuse.struct.TimeManager;
import org.automatedtrade.reuse.struct.TimeSystem;
import org.automatedtrade.reuse.struct.SimulateStreamFromFile;
import org.automatedtrade.reuse.struct.StreamEventPropagator;

/*
 * Parameters to set
 * 
 * --file what file to write to. If not supplied exception
 * 
 * Examples
 * 
 * --file /Users/karejonsson/tradedata/5.trade 
 * 
 */

public class SetupFactory {
	
    public static void main(final String[] args) {
		CommandLineProperties clp = null;
		try {
			clp = PropertiesAssemble.collectCLP(args);
		}
		catch(Exception e) {
			System.out.println("Fel vid inläsningen av kommandoradsparametrarna.");
			e.printStackTrace();
			System.exit(1);						
		}
		try {
			main(clp);
		}
		catch(Exception e) {
			System.out.println("Felaktig terminering.");
			e.printStackTrace();
			System.exit(1);			
		}
		System.exit(0);			
	}
	
	public static void main(final CommandLineProperties clp) throws Exception {
		clp.printEvaluated();
		
		String filename = clp.getString("file", null);
		StreamEventPropagator sel = null;
		
		if(filename == null) {
			sel = setupLive(clp.getString("apikey","de504dc5763aeef9ff52"));
		}
		else {
			sel = setupSimulation(new TimeSystem(), new File(filename));			
		}
		 
        while(true) {
            try {
                Thread.sleep(1000);
            }
            catch (final InterruptedException e) {
                e.printStackTrace();
            }
        }
	}
	
	public static StreamEventPropagator setupSimulation(TimeManager tm, final File file) throws FileNotFoundException {
		StreamEventPropagator ssel = new StreamEventPropagator();
		setupSimulation(ssel, tm, file);
		return ssel;
	}

	public static void setupSimulation(StreamEventPropagator sel, TimeManager tm, final File file) {
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					SimulateStreamFromFile ssff = new SimulateStreamFromFile(sel, tm, file);
					ssff.run();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
	}

	public static StreamEventPropagator setupSimulation(TimeManager tm, final BufferedReader br) throws FileNotFoundException {
		StreamEventPropagator ssel = new StreamEventPropagator();
		setupSimulation(ssel, tm, br);
		return ssel;
	}

	public static void setupSimulation(StreamEventPropagator sel, TimeManager tm, final BufferedReader br) {
		Thread t = new Thread(new Runnable() {
			public void run() {
				try {
					SimulateStreamFromFile ssff = new SimulateStreamFromFile(sel, tm, br);
					ssff.run();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
		});
		t.start();
	}

	public static StreamEventPropagator setupLive(String apiKey) {
		StreamEventPropagator ssel = new StreamEventPropagator();
		setupLive(ssel, apiKey);
		return ssel;
	}

	public static void setupLive(StreamEventPropagator sel, String apiKey) {
		System.out.println("setupLive, apikey = "+apiKey);
		Thread t = new Thread(new Runnable() {
			public void run() {
				StreamLiveFromBitstamp slfb = new StreamLiveFromBitstamp(sel, apiKey);
				slfb.run();
			}
		});
		t.start();
	}

}
