package org.automatedtrade.reuse.struct;

public class TimeSimulationFast implements TimeManager {

	private long time;

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}
	
	public long getDelay(long time) {
		return 0;
	}

}
