package org.automatedtrade.reuse.struct;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Map;

import javax.swing.JOptionPane;

import com.google.gson.Gson;

public class SimulateStreamFromFile {

	private long streamstart;
	private long streamend;
	private long delay;
	private BufferedReader br = null;
	private StreamEventPropagator sel = null;
	private static final Gson gson = new Gson();
	private TimeManager tm;
			
	public SimulateStreamFromFile(StreamEventPropagator sel, TimeManager tm, final File file) throws FileNotFoundException {
		streamstart = System.currentTimeMillis();
	    InputStream fis = new FileInputStream(file);
	    InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
	    br = new BufferedReader(isr);
	    this.sel = sel;
	    this.tm = tm;
	}

	public SimulateStreamFromFile(StreamEventPropagator sel, TimeManager tm, final BufferedReader br) throws FileNotFoundException {
		streamstart = System.currentTimeMillis();
		this.br = br;
	    this.sel = sel;
	    this.tm = tm;
	}

	private void setStreamstart(long time) {
		streamstart = time;
		delay = tm.getDelay(time);//System.currentTimeMillis()-time;
	}

	private void setStreamend(long time) {
		streamend = time;
	}
	
	public void run() {
		try {
			String line;
		    while ((line = br.readLine()) != null) {
		    	doOneLineOfSimulation(sel, line);
		    }
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(br != null) {
			try {
				br.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void doOneLineOfSimulation(final StreamEventPropagator sel, String line) {
		//System.out.println("SimulateStreamFromFile.doOneLineOfSimulation - "+line);
		final int firstBlanc = line.indexOf(' ');
		String firstWord = line.substring(0, firstBlanc);
		String rest = line.substring(firstBlanc+1);
		if(firstWord.startsWith("event")) {
			//System.out.println("SimulatrStream trade "+rest);
			doOneEventSimulation(sel, rest);
			return;
		}
		if(firstWord.startsWith("order_book")) {
			doOneOrderBookSimulation(sel, rest);
			return;
		}
		if(firstWord.startsWith("diff_order_book")) {
			doOneDiffOrderBookSimulation(sel, rest);
			return;
		}
		if(firstWord.startsWith("connectionstatechange")) {
			return;
		}
		if(firstWord.startsWith("subscriptionsucceeded")) {
			return;
		}
		if(firstWord.startsWith("streamstart")) {
			handleStreamStart(rest);
			return;
		}
		if(firstWord.startsWith("streamend")) {
			handleStreamEnd(rest);
			return;
		}
		if(firstWord.startsWith("error")) {
			handleError(rest);
			return;
		}
		
		System.out.println("Line: Har ej implementerat ordet <"+firstWord+"> från raden "+line);
		System.exit(-1);
	}
	
	private void handleStreamStart(String line) {
		setStreamstart(Long.parseLong(line));
	}
	
	private void handleError(String line) {
	}

	private void handleStreamEnd(String line) {
		setStreamend(Long.parseLong(line));
		System.out.println("End of simulation");
		JOptionPane.showMessageDialog(null, "Simulation reached end");
	}

	private void doOneEventSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String firstWord = line.substring(0, firstComma);
		String rest = line.substring(firstComma+2);
		if(firstWord.startsWith("live_orders")) {
			doOneLiveOrderSimulation(sel, rest);
			return;
		}
		if(firstWord.startsWith("order_book")) {
			doOneOrderBookSimulation(sel, rest);
			return;
		}
		if(firstWord.startsWith("diff_order_book")) {
			doOneDiffOrderBookSimulation(sel, rest);
			return;
		}
		if(firstWord.startsWith("live_trades")) {
			//System.out.println("SimlauteStream. live trade "+rest);
			doOneLiveTradeSimulation(sel, rest);
			return;
		}
		System.out.println("Event: Har ej implementerat ordet <"+firstWord+"> från raden "+line);
		System.exit(-1);
	}
	
	private void doOneOrderBookSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String firstWord = line.substring(0, firstComma);
		String rest = line.substring(firstComma+2);
		if(firstWord.startsWith("data")) {
			doOneOrderBookDataSimulation(sel, rest);
			return;
		}
		System.out.println("OrderBook: Har ej implementerat ordet <"+firstWord+"> från raden "+line);
		System.exit(-1);
	}
	
	private void doOneDiffOrderBookSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String firstWord = line.substring(0, firstComma);
		String rest = line.substring(firstComma+2);
		if(firstWord.startsWith("data")) {
			doOneDiffOrderBookDataSimulation(sel, rest);
			return;
		}
		System.out.println("DiffOrderBook: Har ej implementerat ordet <"+firstWord+"> från raden "+line);
		System.exit(-1);
	}
	
	private void doOneLiveTradeSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String firstWord = line.substring(0, firstComma);
		String rest = line.substring(firstComma+2);
		if(firstWord.startsWith("trade")) {
			//System.out.println("SimulateStream trade "+rest);
			doOneLiveTradeTradeSimulation(sel, rest);
			return;
		}
		System.out.println("DiffOrderBook: Har ej implementerat ordet <"+firstWord+"> från raden "+line);
		System.exit(-1);
	}
	
	private void doOneLiveOrderSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String firstWord = line.substring(0, firstComma);
		String rest = line.substring(firstComma+2);
		if(firstWord.startsWith("order_created")) {
			doOneOrderCreatedSimulation(sel, rest);
			return;
		}
		if(firstWord.startsWith("order_deleted")) {
			doOneOrderDeleatedSimulation(sel, rest);
			return;
		}
		if(firstWord.startsWith("order_changed")) {
			doOneOrderChangedSimulation(sel, rest);
			return;
		}
		System.out.println("LiveOrder: Har ej implementerat ordet <"+firstWord+"> från raden "+line);
		System.exit(-1);
	}
	
	private void doOneOrderBookDataSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String timeS = line.substring(0, firstComma);
		long time = Long.parseLong(timeS);
		String rest = line.substring(firstComma+2);
		final Map<String, String> jsonObject = gson.fromJson(rest, Map.class);
		fixTime(jsonObject);
		tm.setTime(time+delay);
		sel.orderbook(jsonObject);
	}
	
	private void doOneDiffOrderBookDataSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String timeS = line.substring(0, firstComma);
		long time = Long.parseLong(timeS);
		String rest = line.substring(firstComma+2);
		final Map<String, String> jsonObject = gson.fromJson(rest, Map.class);
		fixTime(jsonObject);
		tm.setTime(time+delay);
		sel.difforderBook(jsonObject);
	}
	
	private void doOneOrderCreatedSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String timeS = line.substring(0, firstComma);
		long time = Long.parseLong(timeS);
		String rest = line.substring(firstComma+2);
		final Map<String, String> jsonObject = gson.fromJson(rest, Map.class);
		fixTime(jsonObject);
		tm.setTime(time+delay);
		sel.orderCreated(jsonObject);
	}
	
	private void doOneOrderDeleatedSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String timeS = line.substring(0, firstComma);
		long time = Long.parseLong(timeS);
		String rest = line.substring(firstComma+2);
		final Map<String, String> jsonObject = gson.fromJson(rest, Map.class);
		fixTime(jsonObject);
		tm.setTime(time+delay);
		sel.orderDeleted(jsonObject);
	}
	
	private void doOneOrderChangedSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String timeS = line.substring(0, firstComma);
		long time = Long.parseLong(timeS);
		String rest = line.substring(firstComma+2);
		final Map<String, String> jsonObject = gson.fromJson(rest, Map.class);
		fixTime(jsonObject);
		tm.setTime(time+delay);
		sel.orderChanged(jsonObject);
	}
	
	private void doOneLiveTradeTradeSimulation(final StreamEventPropagator sel, String line) {
		final int firstComma = line.indexOf(',');
		String timeS = line.substring(0, firstComma);
		long time = Long.parseLong(timeS);
		String rest = line.substring(firstComma+2);
		final Map<String, String> jsonObject = gson.fromJson(rest, Map.class);
		fixTime(jsonObject);
		tm.setTime(time+delay);
		sel.trade(jsonObject);
	}
	
	private void fixTime(final Map<String, String> jsonObject) {
		String timestamp = jsonObject.get("timestamp");
		if(timestamp != null) {
			long ts = Long.parseLong(timestamp);
			jsonObject.put("timestamp", ""+((long) ((double) (ts+delay)) /1000));
		}
		String datetime = jsonObject.get("datetime");
		if(datetime != null) {
			long ts = Long.parseLong(datetime);
			jsonObject.put("datetime", ""+((long) ((double) (ts+delay)) /1000));
		}
	}
 
}
