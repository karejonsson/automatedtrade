package org.automatedtrade.reuse.struct;

public class StreamLiveFromBitstamp {

	private StreamEventPropagator sep = null;
	private String apiKey = null;
	
	public StreamLiveFromBitstamp(StreamEventPropagator sel, String apiKey) {
	    this.sep = sel;
	    this.apiKey = apiKey;
	}

	public void run() {
		BitstampPusherProxy trades = new BitstampPusherProxy(sep, apiKey, "live_trades", "trade");
    	trades.run();
    	BitstampPusherProxy orders = new BitstampPusherProxy(sep, apiKey, "order_book", "data");
    	orders.run();
    	BitstampPusherProxy book = new BitstampPusherProxy(sep, apiKey, "diff_order_book", "data");
    	book.run();
    	BitstampPusherProxy order_created = new BitstampPusherProxy(sep, apiKey, "live_orders", "order_created");
    	order_created.run();
    	BitstampPusherProxy order_changed = new BitstampPusherProxy(sep, apiKey, "live_orders", "order_changed");
    	order_changed.run();
    	BitstampPusherProxy order_deleted = new BitstampPusherProxy(sep, apiKey, "live_orders", "order_deleted");
    	order_deleted.run();
		//System.out.println("StreamLiveFromBitstamp.run = apiKey = "+apiKey);
	}	

}
