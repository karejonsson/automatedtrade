package org.automatedtrade.reuse.struct;

import java.util.Map;
import java.util.Vector;

public class StreamEventPropagator {
	
	private Vector<TradeListener> tradeListeners = new Vector<TradeListener>();
	
	public void addTradeListener(TradeListener tl) {
		tradeListeners.add(tl);
	}

	public void trade(Map<String, String> map) {
		//System.out.println("StreamEventPropagator.trade("+map+") "+tradeListeners.size()+" to notify");
		for(TradeListener tl : tradeListeners) {
			tl.trade(map);
		}
	}

	private Vector<OrderBooks> orderBooksListeners = new Vector<OrderBooks>();
	
	public void addOrderBooksListener(OrderBooks oc) {
		orderBooksListeners.add(oc);
	}

	public void orderbook(Map<String, String> map) {
		for(OrderBooks tl : orderBooksListeners) {
			tl.orderbook(map);
		}
		//System.out.println("StreamEventPropagator.orderbook("+map+") "+orderBooksListeners.size()+" notified");
	}

	public void difforderBook(Map<String, String> map) {
		for(OrderBooks tl : orderBooksListeners) {
			tl.difforderBook(map);
		}
		//System.out.println("StreamEventPropagator.orderbook("+map+") "+orderBooksListeners.size()+" notified");
	}

	private Vector<OrderCruds> orderCrudsListeners = new Vector<OrderCruds>();
	
	public void addOrderCrudsListener(OrderCruds oc) {
		orderCrudsListeners.add(oc);
	}

	public void orderCreated(Map<String, String> map) {
		for(OrderCruds tl : orderCrudsListeners) {
			tl.orderCreated(map);
		}
		//System.out.println("StreamEventPropagator.orderCreated("+map+") "+orderCrudsListeners.size()+" notified");
	}

	public void orderChanged(Map<String, String> map) {
		for(OrderCruds tl : orderCrudsListeners) {
			tl.orderChanged(map);
		}
		//System.out.println("StreamEventPropagator.orderChanged("+map+") "+orderCrudsListeners.size()+" notified");
	}

	public void orderDeleted(Map<String, String> map) {
		for(OrderCruds tl : orderCrudsListeners) {
			tl.orderDeleted(map);
		}
		//System.out.println("StreamEventPropagator.orderDeleted("+map+") "+orderCrudsListeners.size()+" notified");
	}
	
	public interface TradeListener {
		void trade(Map<String, String> map);
	}

	public interface OrderBooks {
		void orderbook(Map<String, String> map);
		void difforderBook(Map<String, String> map);
	}

	public interface OrderCruds {
		void orderCreated(Map<String, String> map);
		void orderChanged(Map<String, String> map);
		void orderDeleted(Map<String, String> map);
	}

}
