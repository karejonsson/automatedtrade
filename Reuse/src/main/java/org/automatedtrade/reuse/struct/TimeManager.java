package org.automatedtrade.reuse.struct;

public interface TimeManager {
	
	long getTime();
	void setTime(long time);
	long getDelay(long time);
	
}
