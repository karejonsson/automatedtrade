package org.automatedtrade.reuse.struct;

import java.util.Map;

import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionState;
import com.pusher.client.connection.ConnectionStateChange;

public class BitstampPusherProxy implements ConnectionEventListener, ChannelEventListener {
	
    private String apiKey;
    private String channelName;
    private String eventName;
    private PusherOptions options = null;
    private Pusher pusher = null;
    private StreamEventPropagator sel = null;
	private final Gson gson = new Gson();
	
    public BitstampPusherProxy(StreamEventPropagator sel, final String apiKey, String channelName, String eventName) {
    	this.apiKey = apiKey;
    	this.channelName = channelName;
    	this.eventName = eventName;
    	this.sel = sel;
        options = new PusherOptions().setEncrypted(true);
    }
        
    public void run() {
        pusher = new Pusher(apiKey, options);
        pusher.connect(this);
        pusher.subscribe(channelName, this, eventName);
    }

    public void onEvent(String channelName, String eventName, String data) {
		System.out.println("StreamLiveFromBitstamp.onEvent, channelName = "+channelName+", eventName = "+eventName);
        final Map<String, String> map = gson.fromJson(data, Map.class);
        if(channelName.equals("live_orders")) {
	        if(eventName.equals("order_created")) {
	        	sel.orderCreated(map);
	        	return;
	        }
	        if(eventName.equals("order_deleated")) {
	        	sel.orderDeleted(map);
	        	return;
	        }
	        if(eventName.equals("order_changed")) {
	        	sel.orderChanged(map);
	        	return;
	        }
        	return;
        }
        if(channelName.equals("live_trades")) {
	        if(eventName.equals("trade")) {
	        	sel.trade(map);
	        	return;
	        }
        	return;
        }
        if(channelName.equals("order_book")) {
	        if(eventName.equals("data")) {
	        	sel.orderbook(map);
	        	return;
	        }
        	return;
        }
        if(channelName.equals("diff_order_book")) {
	        if(eventName.equals("data")) {
	        	sel.difforderBook(map);
	        	return;
	        }
        	return;
        }
        if(channelName.equals("error")) {
        	strategyOnError();
        	return;
        }
	}
    
    private void strategyOnError() {
		try { pusher.disconnect(); } catch(Exception e) {}
    	while(true) {
    		try {
    			Thread.sleep(Math.max(0, 500));
    		} 
    		catch (InterruptedException e) {
    			e.printStackTrace();
    		}
        	if(pusher != null) {
        		try { pusher.disconnect(); } catch(Exception e) {}
        		pusher = null;
        	}
        	run();
    		try {
    			Thread.sleep(Math.max(0, 500));
    		} 
    		catch (InterruptedException e) {
    			e.printStackTrace();
    		}
    		for(int i = 0 ; i < 6 ; i++) {
        		if(pusher.getConnection().getState() == ConnectionState.CONNECTING) {
            		try {
            			Thread.sleep(Math.max(0, 500));
            		} 
            		catch (InterruptedException e) {
            			e.printStackTrace();
            		}
        		}
    		}
    		if(pusher.getConnection().getState() == ConnectionState.CONNECTED) {
    			break;
    		}
    	}
    }

	public void onSubscriptionSucceeded(String paramString) {
	}

	public void onConnectionStateChange(ConnectionStateChange change) {
	}

	public void onError(String message, String code, Exception e) {
	}
	
}
