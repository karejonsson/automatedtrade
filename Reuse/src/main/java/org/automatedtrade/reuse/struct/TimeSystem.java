package org.automatedtrade.reuse.struct;

public class TimeSystem implements TimeManager {

	public long getTime() {
		return System.currentTimeMillis();
	}
	
	public void setTime(long time) {
		try {
			Thread.sleep(Math.max(0, time-getTime()));
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public long getDelay(long time) {
		return System.currentTimeMillis() - time;
	}

}
