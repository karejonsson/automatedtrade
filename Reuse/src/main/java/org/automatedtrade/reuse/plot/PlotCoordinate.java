package org.automatedtrade.reuse.plot;

public class PlotCoordinate {

	public double horizontal;
	public double vertical;
		
	public String toString() {
		return "PlotCoordinate: Horizontal "+horizontal+", Vertical "+vertical;
	}

}
